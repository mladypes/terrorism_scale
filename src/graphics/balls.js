import * as PIXI from 'pixi.js'

function init (container) {
  const stage = new PIXI.Container()
  const renderer = PIXI.autoDetectRenderer(container.clientWidth, container.clientHeight)
  renderer.backgroundColor = 0x858585
  container.appendChild(renderer.view)
  const texture = PIXI.Texture.fromImage('static/circle.png')
  const particlesContainer = new PIXI.Container()
  stage.addChild(particlesContainer)

  let targetAmount = 0
  let currentAmount = 0
  let step = 0
  let scaleFactor = calculateScaleFactor(renderer)

  const souls = []
  let lastFrameTime

  let frameHandle

  window.addEventListener('resize', resize)

  function resize () {
    renderer.resize(container.clientWidth, container.clientHeight)
    scaleFactor = calculateScaleFactor(renderer)
  }

  function calculateScaleFactor (renderer) {
    const major = renderer.width > renderer.height ? renderer.width : renderer.height
    return major / 2000
  }

  function start () {
    frameHandle = requestAnimationFrame(loop)
    lastFrameTime = performance.now() / 1000
  }

  function loop (time) {
    frameHandle = requestAnimationFrame(loop)
    updateAndDraw(time / 1000)
  }

  function interpolateAndClamp (start, end, ratio) {
    const value = start + (end - start) * ratio
    return value > end ? end : value
  }

  function updateAndDraw (seconds) {
    let delta = (seconds - lastFrameTime); delta
    lastFrameTime = seconds

    for (let i = 0; i < step && currentAmount !== targetAmount; ++i) {
      if (currentAmount < targetAmount) {
        const soul = createSoul()
        soul.creationTime = seconds
        particlesContainer.addChild(soul.sprite)
        souls.push(soul)
        ++currentAmount
      } else {
        particlesContainer.removeChild(particlesContainer.children[particlesContainer.children.length - 1])
        --currentAmount
      }
    }

    souls.forEach((soul, index) => {
      soul.sprite.position.x = soul.position.x
      soul.sprite.position.y = soul.position.y
      const interpolatedScaleFactor = interpolateAndClamp(0, scaleFactor, (seconds - soul.creationTime) * 10)
      const scale = interpolatedScaleFactor + interpolatedScaleFactor * ((1 + Math.sin(soul.offset * 10 + seconds)) / 2)
      soul.sprite.scale.x = scale
      soul.sprite.scale.y = scale
    })
    renderer.render(stage)
  }

  function setCount (value) {
    targetAmount = value

    step = Math.ceil(Math.abs(currentAmount - targetAmount) / 30)
  }

  function createSoul () {
    const sprite = new PIXI.Sprite(texture)
    sprite.scale.x = 0.1 + 0.1 * Math.random()
    sprite.scale.y = sprite.scale.x
    sprite.tint = 0xf0f30b * (1 + Math.random() * 0.00001)
    sprite.anchor.set(0.5, 0.5)

    const soul = {
      position: {
        x: Math.random() * renderer.width,
        y: Math.random() * renderer.height
      },
      offset: Math.random(),
      sprite
    }

    return soul
  }

  function clear () {
    window.removeEventListener('resize', resize)
    cancelAnimationFrame(frameHandle)
    renderer.destroy()
  }

  return {
    start,
    setCount,
    clear
  }
}

export {
  init
}
