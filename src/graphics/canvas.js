function createCanvas2d (width, height) {
  const element = document.createElement('canvas')
  const dpr = window.devicePixelRatio

  element.width = width * dpr
  element.height = height * dpr

  element.style.width = `${width}px`
  element.style.height = `${height}px`

  const context = element.getContext('2d')
  context.scale(dpr, dpr)

  return {
    element,
    context,
    width,
    height
  }
}

export {
  createCanvas2d
}
